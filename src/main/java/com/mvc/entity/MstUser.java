package com.mvc.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the MST_USER database table.
 * 
 */
@Entity
@Table(name="MST_USER")
@NamedQuery(name="MstUser.findAll", query="SELECT m FROM MstUser m")
public class MstUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int userID;

	private String alamat;

	private String jenisKelamin;

	private String nama;

	private String noTelpon;

	private String password;

	private Timestamp tanggalLahir;

	private String username;

	public MstUser() {
	}

	public int getUserID() {
		return this.userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getAlamat() {
		return this.alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getJenisKelamin() {
		return this.jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getNama() {
		return this.nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNoTelpon() {
		return this.noTelpon;
	}

	public void setNoTelpon(String noTelpon) {
		this.noTelpon = noTelpon;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getTanggalLahir() {
		return this.tanggalLahir;
	}

	public void setTanggalLahir(Timestamp tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}